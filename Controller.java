package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


import javax.imageio.ImageIO;
import javax.naming.Context;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

public class Controller {

    @FXML
    private Canvas canvas;


    @FXML
    private Label label;

    @FXML
    private Slider slider;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private Button clearC;
    @FXML
    private Button erazer;

    @FXML
    private MenuItem openImage;

    public void initialize() {

        final GraphicsContext g = canvas.getGraphicsContext2D();

        erazer.setOnAction(ActionEvent -> {
            g.setStroke(Color.rgb(242,242,242));
        });


        openImage.setOnAction(actionEvent -> {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select Some File");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png"),
                    new FileChooser.ExtensionFilter("JPEG files (*.jpeg)", ".*jpeg"),
                    new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg"));

            fileChooser.setInitialFileName("saved File");


            fileChooser.setInitialDirectory(new File("D:\\simplePaint\\res"));
           // File file = fileChooser.showOpenDialog(null);
            File selectedFile =fileChooser.showOpenDialog(null);
            if (selectedFile != null) {

                String imagePath = "file:\\D:\\simplePaint\\res\\saved File.png";
                Image image = new Image(imagePath);
// Draw the Image
                //g.drawImage(image, 10, 10, 200, 200);
                g.drawImage(image,  0,0, 537.0 ,600);
            }
        });

        clearC.setOnAction(actionEvent -> {
            g.clearRect(0, 0, 800,900);
        });

        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                label.setText(String.format("%.0f", newValue));
                g.setLineWidth(slider.getValue());
            }
        });

        colorPicker.setOnAction(event -> {
            g.setStroke(colorPicker.getValue());

        });

        canvas.setOnMousePressed(event -> {
            g.beginPath();
            g.lineTo(event.getX(), event.getY());
            g.stroke();
        });

        canvas.setOnMouseDragged(event -> {
            g.lineTo(event.getX(), event.getY());
            g.stroke();
        });
    }

    public void onSave() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPEG files (*.jpeg)", ".*jpeg"),
                new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg"));

        fileChooser.setInitialFileName("saved File");
        fileChooser.setInitialDirectory(new File("D:\\simplePaint\\res"));
        File file = fileChooser.showSaveDialog(null);
        if (file != null) {
            try {
                Image image = canvas.snapshot(null, null);
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            } catch (IOException ex) {
                System.out.println("Failed to save image!");
            }
        }
    }

    public void onExit() {
        Platform.exit();
    }
    /*public void onOpen(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Some File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPEG files (*.jpeg)", ".*jpeg"),
                new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg"));

        fileChooser.setInitialFileName("saved File");


        fileChooser.setInitialDirectory(new File("D:\\simplePaint\\res"));
        File file = fileChooser.showOpenDialog(null);
        File selectedFile =fileChooser.showOpenDialog(null);
        if (selectedFile != null) {
            Main.primaryStage.(selectedFile);
            String imagePath = "file:\\Path-To-Your-Image\\java-logo.gif";
            Image image = new Image(imagePath);
// Draw the Image
            gc.drawImage(image, 10, 10, 200, 200);
            gc.drawImage(image, 220, 50, 100, 70);
        }

    }*/
   @FXML
    void clearCanvas(ActionEvent event) {
      //  g.clearRect(0, 0, 800,900);
    }

}
